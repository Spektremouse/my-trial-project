﻿namespace ClassLibrary1
{
    public class SimpleClass
    {
        public int Add(int x, int y)
        {
            return x + y;
        }

        public decimal Divide(int x, int y)
        {
            return x / y;
        }
    }
}
