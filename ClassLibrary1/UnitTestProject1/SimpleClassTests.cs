﻿using System;
using ClassLibrary1;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    [TestClass]
    public class SimpleClassTests
    {
        [TestMethod]
        public void Add_PositiveIntegers()
        {
            int x = 2;
            int y = 2;
            int expected = 4;

            var simple = new SimpleClass();
            int actual = simple.Add(x, y);

            Assert.AreEqual(expected, actual);
        }
    }
}
